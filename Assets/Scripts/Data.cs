﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data : MonoBehaviour {

    public static int Tank1HP;
    public static int Tank2HP;
    public int TanksHP = 5;
    public static bool Tank1GameOver;
    public static bool Tank2GameOver;

    public static bool Tank1MoveForwardBool;
    public static bool Tank1MoveBackwardBool;
    public static bool Tank1MoveLeftBool;
    public static bool Tank1MoveRightBool;
    public static bool Tank1ShootBool;

    public static bool Tank2MoveForwardBool;
    public static bool Tank2MoveBackwardBool;
    public static bool Tank2MoveLeftBool;
    public static bool Tank2MoveRightBool;
    public static bool Tank2ShootBool;


    void Start () {
        Tank1HP = TanksHP;
        Tank2HP = TanksHP;
        Tank1GameOver = false;
        Tank2GameOver = false;
    }
	
	void Update () {
        if (Tank1HP < 1)
            Tank1GameOver = true;

        if (Tank2HP < 1)
            Tank2GameOver = true;

    }

    public void Tank1MoveForward()
    {
        Tank1MoveForwardBool = true;
    }

    public void Tank1DontMoveForward()
    {
        Tank1MoveForwardBool = false;
    }

    public void Tank1MoveBackward()
    {
        Tank1MoveBackwardBool = true;
    }

    public void Tank1DontMoveBackward()
    {
        Tank1MoveBackwardBool = false;
    }


    public void Tank1MoveLeft()
    {
        Tank1MoveLeftBool = true;
    }

    public void Tank1DontMoveLeft()
    {
        Tank1MoveLeftBool = false;
    }

    public void Tank1MoveRight()
    {
        Tank1MoveRightBool = true;
    }

    public void Tank1DontRight()
    {
        Tank1MoveRightBool = false;
    }

    public void Tank1Shoot()
    {
        Tank1ShootBool = true;
    }

    public void Tank1DontShoot()
    {
        Tank1ShootBool = false;
    }





}
