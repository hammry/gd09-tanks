﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {

    public bool IsTank1;
    public GameObject Prefab;
    public GameObject Smoke;
    public int Speed = 25;
    public AudioSource GunSound;
    float GunTimer = 0;

	void Start () {
        GunSound = this.gameObject.GetComponent<AudioSource>();
    }
	
	void Update () {

        if (IsTank1)
        {
            GunTimer += Time.deltaTime;

            if ((Data.Tank1ShootBool || Input.GetKeyDown("joystick 1 button 5") || Input.GetKeyDown(KeyCode.Space)) && GunTimer > 3)
            {
                GunTimer = 0;
                GunSound.Play();
                GameObject Bullet = Instantiate(Prefab, transform.position, transform.rotation);
                Bullet.GetComponent<Rigidbody>().velocity = -transform.forward * Speed;

                GameObject NewSmoke = Instantiate(Smoke, transform.position, transform.rotation);
                NewSmoke.transform.parent = transform;

                Destroy(Bullet, 5);
                Destroy(NewSmoke, 3);
            }
        }
        else
        {
            GunTimer += Time.deltaTime;

            if ((Input.GetKeyDown("joystick 2 button 5") || Input.GetKeyDown(KeyCode.RightControl)) && GunTimer > 3)

            {
                GunTimer = 0;
                GunSound.Play();
                GameObject Bullet = Instantiate(Prefab, transform.position, transform.rotation);
                Bullet.GetComponent<Rigidbody>().velocity = -transform.forward * Speed;

                GameObject NewSmoke = Instantiate(Smoke, transform.position, transform.rotation);
                NewSmoke.transform.parent = transform;

                Destroy(Bullet, 5);
                Destroy(NewSmoke, 3);
            }

        }
       
	}
}
