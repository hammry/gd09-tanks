﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletExplode : MonoBehaviour {

    public GameObject Fire;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<TankMove>())
        if (collision.gameObject.GetComponent<TankMove>().IsTank1)
            Data.Tank1HP = Data.Tank1HP - 1;
        else
            Data.Tank2HP = Data.Tank2HP - 1;

        GameObject FX = Instantiate(Fire, transform.position, transform.rotation);
        Destroy(FX, 2);
        Destroy(this.gameObject);
    }

}
