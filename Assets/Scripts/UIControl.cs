﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIControl : MonoBehaviour {

    public Text Tank1HPtext;
    public Text Tank2HPtext;
    public GameObject GameOver;
    public Text GameOverText;
    public GameObject PauseMenu;
    public bool Paused = false;


    void Start () {
        GameOver.SetActive(false);

        PauseMenu.SetActive(false);
        Time.timeScale = 1;
        AudioListener.volume = 1;
        Paused = false;
    }
	

	void Update () {
        Tank1HPtext.text = "Tank 1 HP = " + Data.Tank1HP;
        Tank2HPtext.text = "Tank 2 HP = " + Data.Tank2HP;


        if (Data.Tank1GameOver)
        {
            GameOver.SetActive(true);
            GameOverText.text = "GAMEOVER \n TANK1 ЛОПУХ";
        }

        if (Data.Tank2GameOver)
        {
            GameOver.SetActive(true);
            GameOverText.text = "GAMEOVER \n TANK2 ЛОПУХ";
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Paused)
            {
                PauseMenu.SetActive(false);
                Time.timeScale = 1;
                AudioListener.volume = 1;
                Paused = false;

            }
            else
            {
               Time.timeScale = 0;
                PauseMenu.SetActive(true);
                Paused = true;
                AudioListener.volume = 0;
            }
        }

    }

    public void Restart()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void OpenMenu()
    {
        Application.LoadLevel("Menu");
    }


}
