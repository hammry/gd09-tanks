﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class TankMove : MonoBehaviour {

    public bool IsTank1;
    Rigidbody Tank;
    public int MovePower = 200;
    public int RotatePower = 100;

    public GameObject Gun;

    void Start () {
        Tank = this.gameObject.GetComponent<Rigidbody>();
        Tank.mass = 10;
        MovePower = 200;
        RotatePower = 1200;
    }
	
	void FixedUpdate () {

        if (IsTank1)
        {
            if (Input.GetKey(KeyCode.W) || Data.Tank1MoveForwardBool)
                Tank.AddForce(-transform.forward * MovePower);

            if (Input.GetKey(KeyCode.S) || Data.Tank1MoveBackwardBool)
                Tank.AddForce(transform.forward * MovePower);

            if (Input.GetKey(KeyCode.D) || Data.Tank1MoveRightBool)
                Tank.AddTorque(Vector3.up * RotatePower);

            if (Input.GetKey(KeyCode.A) || Data.Tank1MoveLeftBool)
                Tank.AddTorque(-Vector3.up * RotatePower);

            if (Input.GetKey(KeyCode.Q))
                Gun.transform.eulerAngles = new Vector3(Gun.transform.rotation.x,
                    Gun.transform.eulerAngles.y - 1, Gun.transform.rotation.z);

            if (Input.GetKey(KeyCode.E))
                Gun.transform.eulerAngles = new Vector3(Gun.transform.rotation.x,
                    Gun.transform.eulerAngles.y + 1, Gun.transform.rotation.z);



            if (Input.GetAxis("Joy1-A1") > 0.1f)
                Tank.AddTorque(Vector3.up * RotatePower * Input.GetAxis("Joy1-A1"));

            if (Input.GetAxis("Joy1-A1") < -0.1f)
                Tank.AddTorque(Vector3.up * RotatePower * Input.GetAxis("Joy1-A1"));

            if (Input.GetAxis("Joy1-A2") < -0.1f)
                Tank.AddForce(transform.forward * MovePower * Input.GetAxis("Joy1-A2"));

            if (Input.GetAxis("Joy1-A2") > 0.1f)
                Tank.AddForce(transform.forward * MovePower * Input.GetAxis("Joy1-A2"));

            Gun.transform.eulerAngles = new Vector3(Gun.transform.rotation.x,
                Gun.transform.eulerAngles.y + Input.GetAxis("Joy1-A4"), Gun.transform.rotation.z);


        }
        else
        {
            if (Input.GetKey(KeyCode.UpArrow))
                Tank.AddForce(-transform.forward * MovePower);

            if (Input.GetKey(KeyCode.DownArrow))
                Tank.AddForce(transform.forward * MovePower);

            if (Input.GetKey(KeyCode.RightArrow))
                Tank.AddTorque(Vector3.up * RotatePower);

            if (Input.GetKey(KeyCode.LeftArrow))
                Tank.AddTorque(-Vector3.up * RotatePower);

            if (Input.GetKey(KeyCode.PageUp))
                Gun.transform.eulerAngles = new Vector3(Gun.transform.rotation.x,
                    Gun.transform.eulerAngles.y - 1, Gun.transform.rotation.z);

            if (Input.GetKey(KeyCode.PageDown))
                Gun.transform.eulerAngles = new Vector3(Gun.transform.rotation.x,
                    Gun.transform.eulerAngles.y + 1, Gun.transform.rotation.z);

            if (Input.GetAxis("Joy2-A1") > 0.1f)
                Tank.AddTorque(Vector3.up * RotatePower * Input.GetAxis("Joy2-A1"));

            if (Input.GetAxis("Joy2-A1") < -0.1f)
                Tank.AddTorque(Vector3.up * RotatePower * Input.GetAxis("Joy2-A1"));

            if (Input.GetAxis("Joy2-A2") < -0.1f)
                Tank.AddForce(transform.forward * MovePower * Input.GetAxis("Joy2-A2"));

            if (Input.GetAxis("Joy2-A2") > 0.1f)
                Tank.AddForce(transform.forward * MovePower * Input.GetAxis("Joy2-A2"));

            Gun.transform.eulerAngles = new Vector3(Gun.transform.rotation.x,
                Gun.transform.eulerAngles.y + Input.GetAxis("Joy2-A3"), Gun.transform.rotation.z);

        }      
    }
}
